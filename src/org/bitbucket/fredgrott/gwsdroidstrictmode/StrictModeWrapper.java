/**
 * 
 */
package org.bitbucket.fredgrott.gwsdroidstrictmode;


import android.content.Context;
import android.content.pm.ApplicationInfo;

import android.os.StrictMode;
import android.util.Log;

// TODO: Auto-generated Javadoc
/**
 * The Class StrictModeWrapper.
 * 
 * @author fredgrott 
 * 
 * StrictModeWrapper class, since StrictMode is a performance
 *         tuning set of classes and methods and usually we want only call once
 *         for a java class file we want a flexible solution that allows us to
 *         reduce the number of repetition of calls to a method while at the
 *         same time allowing us to do such things as do a noop when anything
 *         below android 2.3 is targeted. In this case using a set of
 *         reflections is not advantageous due to the slow down caused by
 *         reflection. So we use something on the order of lazy loading but not
 *         the method pioneered by Block as we only have to check to see if its
 *         targeting android 2.3 or higher. The other consideration is that
 *         Dianne Hackborn stated that we should sparingly use techniques that
 *         extend the application class and thus a wrapper is more Desirable
 *         than extending the application class to enable strict mode by calling
 *         it in the onCreate of our activity class rather than in our extension
 *         of the application class,. such as: 
 *         
 *         <code>
 * 
 * onCreate {
 * StrictModeWrapper.init(this);
 * 
 * 
 * }
 * </code> 
 * 
 * Notice that we also avoid throwing an exception, as it is much
 * cleaner this way. The original technical discussion where concepts
 * are borrowed is from stackoverflow: 
 * 
 * <a href=
 *         "http://stackoverflow.com/questions/4610627/strictmode-for-lower-platform-versions"
 *         >
 *         http://stackoverflow.com/questions/4610627/strictmode-for-lower-platform
 *         -versions</a> 
 *         
 * I borrowed code and concepts from DaveMacLean and
 * extended his concepts and code. To enable a more flexible set of
 * policies one would need to extend this wrapper class and add if
 * statements blocking out the new policies per targeted api using
 * SDK_INT Thus, it would be 
 *         
 *         <code>
 * static int SDK_INT = android.os.Build.VERSION.SDK_INT;
 * onCreate {
 * if ((applicationFlags & ApplicationInfo.FLAG_DEBUGGABLE) != 0 || (SDK_INT>8) || (SDK_INT<10)) {
 * StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
 * .somethingToBechecked()
 * .penaltyLog()
 * .build());
 * StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
 * .somethingtobeChecked()
 * .penaltyLog()
 * .build());
 * } If ((check next target level) {
 * 
 * }
 * 
 * else {
 * Log.v("StrictMode", "... is not available. Punting...");
 * }
 * }
 * </code> 
 * 
 * Thus, this is only a temporary wrapper to be used temporary when you
 * first start debugging for performance and it SHOULD NOT be set as
 * default in an activity helper class as an example or an extended
 * application class.
 * 
 * Remember, setTHreadPolicy base policy is API 9 but setVMPolicy base policy is 
 * API 11. Thus on API 9 up you can right away set a base ThreadPolicy and add 
 * to it but on VMPolicy you use a minimum as you cannot add to a base policy until
 * API 11.  In shorter words my wrapper is set to the minimum thus use it 
 * until you have start specifying different policy deviations than 
 * remove the call to my wrapper and use your own detaield calls.
 * 
 * See the xmind/freemind diagram in both xmind nd png forms in the 
 * project source.
 * 
 */
public class StrictModeWrapper {
    
    /** The sdk int. */
    static int SDK_INT = android.os.Build.VERSION.SDK_INT;
    
    /**
     * Inits the.
     * 
     * @param context the context
     */
    public static void init(Context context) {
        // check if android:debuggable is set to true and if target is higher than api 8
        int applicationFlags = context.getApplicationInfo().flags;
        if ((applicationFlags & ApplicationInfo.FLAG_DEBUGGABLE) != 0 || (SDK_INT>8)) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build());
        } else {
            Log.v("StrictMode", "... is not available. Punting...");
        }
    }

}

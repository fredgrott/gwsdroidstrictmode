![Strcit Mode image](https://lh4.googleusercontent.com/-NGk7SGyoSa4/UMig8RRCOjI/AAAAAAAAA4U/8QdA0T2nbnQ/s221/S_Strict_teacher.gif)

GWSDroidStrictMode
---

Basic StrcitMode wrapper for android application development use in the 
form of android project library.

# Project License

[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.html)

# Project Credits

## Fred Grott

[Fred Grott's blog](http://grottworkshop.blogspot.com)

[Fred Grott's website](http://fredgrott.bitbucket.org)


